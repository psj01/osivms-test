import React, { Component } from "react";
import "./App.css";
import { Route, Switch } from "react-router-dom";
import NavBar from "./components/navBar";
import VmsHome from "./components/home";
import Fatclaims from "./components/fatclaims";
import Ncrs from "./components/ncrs";
import Microresults from "./components/microResults";
import Inspections from "./components/inspections";
import Fatclaim from "./components/fatclaim";
import Ncr from "./components/ncr";
import Microresult from "./components/microresult";
import Inspection from "./components/inspection";
class App extends Component {
  render() {
    return (
      <div className="App">
        <NavBar />
        <div className="content">
          <Switch>
            <Route path="/fatclaims/:id" component={Fatclaim} />
            <Route path="/fatclaims" component={Fatclaims} />
            <Route path="/ncrs/:id" component={Ncr} />
            <Route path="/ncrs" component={Ncrs} />
            <Route path="/microresults/:id" component={Microresult} />
            <Route path="/microresults" component={Microresults} />
            <Route path="/inspections/:id" component={Inspection} />
            <Route path="/inspections" component={Inspections} />
            <Route path="/" component={VmsHome} />
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
