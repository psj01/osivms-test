import React, { Component } from "react";
import { Link } from "react-router-dom";
class NavBar extends Component {
  state = {
    options1: "none",
    options2: "none",
    options3: "none",
    options4: "none"
  };

  handleMouseOver = e => {
    let x = e.currentTarget.id;
    if (x === "fc") this.setState({ options1: "block" });
    if (x === "ncrs") this.setState({ options2: "block" });
    if (x === "microresults") this.setState({ options3: "block" });
    if (x === "inspections") this.setState({ options4: "block" });
  };

  handleMouseLeave = e => {
    let x = e.currentTarget.id;
    if (x === "fc") this.setState({ options1: "none" });
    if (x === "ncrs") this.setState({ options2: "none" });
    if (x === "microresults") this.setState({ options3: "none" });
    if (x === "inspections") this.setState({ options4: "none" });
  };

  render() {
    return (
      <div>
        <table className="nav">
          <tbody>
            <tr>
              <td rowSpan="2">
                <Link to="/">
                  <img src="./osiLogo.jpg" alt="osi logo" />
                </Link>
              </td>
              <td colSpan="5">
                <img src="./vmsSlogan.gif" alt="vms slogan" />
              </td>
            </tr>
            <tr className="navLinks">
              <td
                id="fc"
                onMouseOver={e => this.handleMouseOver(e)}
                onMouseLeave={e => this.handleMouseLeave(e)}
              >
                <Link to="/fatclaims">
                  <img src="./buttonFC.gif" alt="button" />
                </Link>
                <table
                  className="submenu1"
                  style={{ display: this.state.options1 }}
                >
                  <tbody>
                    <tr>
                      <td>
                        <Link to="/fatclaims/1">fat1</Link>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <Link to="/fatclaims/2">fatclaims2</Link>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <Link to="/fatclaims/3">fatclaims3</Link>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
              <td
                id="ncrs"
                onMouseOver={e => this.handleMouseOver(e)}
                onMouseLeave={e => this.handleMouseLeave(e)}
              >
                <Link to="/ncrs">
                  <img src="./buttonNCR.gif" alt="button" />
                </Link>
                <table
                  className="submenu2"
                  style={{ display: this.state.options2 }}
                >
                  <tbody>
                    <tr>
                      <td>
                        <Link to="/ncrs/1">ncr1</Link>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <Link to="/ncrs/2">ncr2</Link>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <Link to="/ncrs/3">ncr3</Link>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
              <td
                id="microresults"
                onMouseOver={e => this.handleMouseOver(e)}
                onMouseLeave={e => this.handleMouseLeave(e)}
              >
                <Link to="/microresults">
                  <img src="./buttonMicro.gif" alt="button" />
                </Link>

                <table
                  className="submenu3"
                  style={{ display: this.state.options3 }}
                >
                  <tbody>
                    <tr>
                      <td>
                        <Link to="/microresults/1">microResults1</Link>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <Link to="/microresults/2">microResults2</Link>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <Link to="/microresults/3">microResults3</Link>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
              <td
                id="inspections"
                onMouseOver={e => this.handleMouseOver(e)}
                onMouseLeave={e => this.handleMouseLeave(e)}
              >
                <Link to="/inspections">
                  <img src="./buttonInsp.gif" alt="button" />
                </Link>
                <table
                  className="submenu4"
                  style={{ display: this.state.options4 }}
                >
                  <tbody>
                    <tr>
                      <td>
                        <Link to="/inspections/1">insp1</Link>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <Link to="/inspections/2">insp2</Link>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <Link to="/inspections/3">insp3</Link>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

export default NavBar;
